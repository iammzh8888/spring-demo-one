package com.polymtl.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {

	public static void main(String[] args) {
		// 1. load the spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// 2. retrieve bean from spring container
		ICoach theCoach = context.getBean("myCoach",ICoach.class);
		
		// 3. call methods on the bean
		System.out.println(theCoach.getDailyWorkout());
		
		// call the new method for fortunes
		System.out.println(theCoach.getDailyFortune());
		
		// 4. close the context
		context.close();

		String str = "dsafgsgds";
		str.substring(0,2);
str.contains("ssd");

	}

}
