package com.polymtl.springdemo;

public interface ICoach {
	public String getDailyWorkout();

	public String getDailyFortune();
}
