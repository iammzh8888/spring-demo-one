package com.polymtl.springdemo;

public class TrackCoach implements ICoach {
	
	//define a private field for dependency
	private IFortuneService fortuneService;
		
	//define a constructor for dependency
	public TrackCoach(IFortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Run a hard 5K";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return "Just do it:" + fortuneService.getFortune();
	}

}
